<?php

namespace Xn\CodeMirrorEditor;

use Xn\Admin\Extension;

class CodeMirrorExtension extends Extension
{
    const ASSETS_PATH = 'vendor/laravel-admin-ext/code-mirror-editor/codemirror-5.65.9/';

    public $name = "laravel-admin-code-mirror-editor";

    public $views = __DIR__.'/../resources/views';

    public $assets = __DIR__.'/../resources/assets';
}