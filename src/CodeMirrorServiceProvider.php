<?php

namespace Xn\CodeMirrorEditor;

use Xn\Admin\Admin;
use Xn\Admin\Form;
use Illuminate\Support\ServiceProvider;

class CodeMirrorServiceProvider extends ServiceProvider
{
    /**
     * {@inheritdoc}
     */
    public function boot(CodeMirrorExtension $extension)
    {
        if (! CodeMirrorExtension::boot()) {
            return ;
        }

        $this->loadViewsFrom(resource_path('views')."/vendor/xn/code-mirror-editor/views", $extension->name);

        $this->registerPublishing($extension);

        Admin::booting(function () {
            Form::extend('codeMirrorEditor', Editor::class);
            Form::alias('codeMirrorEditor', 'cme');

            Form::extend('cmeHtml', HtmlEditor::class);
            // Form::extend('jsond', Jsond::class);
            // Form::extend('typescript', Typescript::class);
        });
    }

    /**
     * Register the package's publishable resources.
     *
     * @return void
     */
    protected function registerPublishing(CodeMirrorExtension $extension)
    {
        if ($this->app->runningInConsole()) {
            $views = $extension->views();
            $assets = $extension->assets();
            $this->publishes(
                [
                    $views => resource_path('views')."/vendor/xn/code-mirror-editor/views",
                    $assets => public_path('vendor/laravel-admin-ext/code-mirror-editor')
                ], 
                $extension->name
            );
        }
    }
}
